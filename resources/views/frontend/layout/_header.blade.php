<header class="header-section clearfix">
    <div class="container-fluid">
        <a href="/" class="site-logo">
            <img src="/frontend/img/logo.png" alt="">
        </a>
        <div class="responsive-bar"><i class="fa fa-bars"></i></div>
        <a href="" class="user"><i class="fa fa-user"></i></a>
        @auth
            <a href="{{ route('dashboard')}}" class="site-btn">{{auth()->user()->firstname}} </a>
        @else
            <a href="{{ route('login')}}" class="site-btn">Login </a>
        @endauth

        <nav class="main-menu">
            <ul class="menu-list">
                <li><a href="/#feature">Features</a></li>
                <li><a href="/#blog">Blog</a></li>
                <li><a href="/#about">About</a></li>
                <li><a href="/#team">Our Team</a></li>
                <li><a href="{{ route('contact')}}">Contact</a></li>
                @auth
                <a href="{{ route('dashboard')}}" class="btn btn-primary btn-block text-white mb-4 nav-btn">{{auth()->user()->firstname}}</a>
                @endauth
                @guest
                <a href="{{ route('login')}}" class="btn btn-primary btn-block text-white mb-4 nav-btn">Login</a>
                @endguest
            </ul>
        </nav>
    </div>
</header>
