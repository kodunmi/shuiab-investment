<!DOCTYPE html>
<html lang="en">
<head>
	<title>Cryptocurrency - Landing Page Template</title>
	<meta charset="UTF-8">
	<meta name="description" content="Cryptocurrency Landing Page Template">
	<meta name="keywords" content="cryptocurrency, unica, creative, html">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Favicon -->
	<link href="img/favicon.ico" rel="shortcut icon"/>

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="/frontend/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="/frontend/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="/frontend/css/themify-icons.css"/>
	<link rel="stylesheet" href="/frontend/css/animate.css"/>
	<link rel="stylesheet" href="/frontend/css/owl.carousel.css"/>
	<link rel="stylesheet" href="/frontend/css/style.css"/>


	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Header section -->
	@include('frontend.layout._header')
	<!-- Header section end -->

    @yield('content')

	<!-- Footer section -->
    @include('frontend.layout._footer')


	<!--====== Javascripts & Jquery ======-->
	<script src="/frontend/js/jquery-3.2.1.min.js"></script>
	<script src="/frontend/js/owl.carousel.min.js"></script>
	<script src="/frontend/js/main.js"></script>
</body>
</html>
