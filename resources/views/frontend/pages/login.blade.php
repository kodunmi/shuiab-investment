@extends('frontend.layout._master')
@section('content')
<!-- Page info section -->
	<section class="page-info-section">
		<div class="container">
			<h2>Login</h2>
			<div class="site-beradcamb">
				<a href="{{ route('home')}}">Home</a>
				<span><i class="fa fa-angle-right"></i> Login</span>
			</div>
		</div>
	</section>
	<!-- Page info end -->



	<!-- Contact section -->
	<section class="contact-page spad">
		<div class="container">
            <h3 class="text-center mb-4">Login</h3>
			<div class="row">
				<div class="col-lg-7 offset-lg-3">
					<form class="contact-form" method="POST" action="{{ route('login.submit')}}">
                        @csrf
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<input class="check-form" type="email" name="email" placeholder="Email Address" required value="{{old('email')}}">
                                    <span><i class="ti-check"></i></span>
                                    @error('email')
                                        <lable class="text-danger text-center">{{$message}}</lable>
                                    @enderror

								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<input class="check-form" type="password"  name="password" required placeholder="Password">
                                    <span><i class="ti-check"></i></span>
                                    @error('password')
                                        <lable class="text-danger text-center">{{ $message }}</lable>
                                    @enderror

								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
                                    <button class="site-btn sb-gradients mt-4">Login</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
	<!-- Contact section end -->


	<!-- Newsletter section -->
	<section class="newsletter-section gradient-bg">
		<div class="container text-white">
			<div class="row">
				<div class="col-lg-7 newsletter-text">
					<h2>Subscribe to our Newsletter</h2>
					<p>Sign up for our weekly industry updates, insider perspectives and in-depth market analysis.</p>
				</div>
				<div class="col-lg-5 col-md-8 offset-lg-0 offset-md-2">
					<form class="newsletter-form">
						<input type="text" placeholder="Enter your email">
						<button>Get Started</button>
					</form>
				</div>
			</div>
		</div>
	</section>
	<!-- Newsletter section end -->
@endsection
