@extends('frontend.layout._master')
@section('content')
<!-- Page info section -->
	<section class="page-info-section">
		<div class="container">
			<h2>Dashboard</h2>
			<div class="site-beradcamb">
				<a href="{{ route('home')}}">Home</a>
				<span><i class="fa fa-angle-right"></i> Dashbooard</span>
			</div>
		</div>
	</section>
	<!-- Page info end -->

	<!-- Contact section -->
	<section class="contact-page spad">
		<div class="container">
            <h3 class="text-center mb-4">Welcome Back {{auth()->user()->firstname}}</h3>
            <div class="col-md-4 offset-md-4 process">
                <div class="process-step">
                    <figure class="process-icon">
                        <img src="/frontend/img/process-icons/1.png" alt="#">
                    </figure>
                    <h4>Below Is Your Investment Number</h4>
                    <p>Your investment number is a unique number only you can see, and it should be kept save</p>
                    <h4 class="mt-2 "> <strong>{{auth()->user()->investment_number}}</strong> </h4>
                    <br>
                    <a href="{{route('logout')}}" class="site-btn sb-gradients sbg-line">logout</a>
                </div>
            </div>

		</div>
	</section>
	<!-- Contact section end -->


	<!-- Newsletter section -->
	<section class="newsletter-section gradient-bg">
		<div class="container text-white">
			<div class="row">
				<div class="col-lg-7 newsletter-text">
					<h2>Subscribe to our Newsletter</h2>
					<p>Sign up for our weekly industry updates, insider perspectives and in-depth market analysis.</p>
				</div>
				<div class="col-lg-5 col-md-8 offset-lg-0 offset-md-2">
					<form class="newsletter-form">
						<input type="text" placeholder="Enter your email">
						<button>Get Started</button>
					</form>
				</div>
			</div>
		</div>
	</section>
	<!-- Newsletter section end -->
@endsection
