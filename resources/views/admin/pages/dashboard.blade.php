@extends('admin.layout._master')
@section('content')

    <div class="content-wrapper">
        <div class="row" id="proBanner">
            <div class="col-12">
                <span class="d-flex align-items-center purchase-popup">
                    <p>Like what you see? Check out our premium version for more.</p>
                    <a href="https://github.com/BootstrapDash/PurpleAdmin-Free-Admin-Template" target="_blank"
                        class="btn ml-auto download-button">Download Free Version</a>
                    <a href="https://www.bootstrapdash.com/product/purple-bootstrap-4-admin-template/" target="_blank"
                        class="btn purchase-button">Upgrade To Pro</a>
                    <i class="mdi mdi-close" id="bannerClose"></i>
                </span>
            </div>
        </div>
        <div class="page-header">
            <h3 class="page-title">
                <span class="page-title-icon bg-gradient-primary text-white mr-2">
                    <i class="mdi mdi-home"></i>
                </span> Dashboard </h3>
            <nav aria-label="breadcrumb">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item active" aria-current="page">
                        <span></span>Overview <i
                            class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="row">
            <div class="col-md-4 stretch-card grid-margin">
                <div class="card bg-gradient-danger card-img-holder text-white">
                    <div class="card-body">
                        <img src="/admin/assets/images/dashboard/circle.svg" class="card-img-absolute"
                            alt="circle-image" />
                        <h4 class="font-weight-normal mb-3">Registered User <i
                                class="mdi mdi-chart-line mdi-24px float-right"></i>
                        </h4>
                        <h2 class="mb-5">{{ $users->count()}}</h2>
                        <h6 class="card-text">Increased by 60%</h6>
                    </div>
                </div>
            </div>
            <div class="col-md-4 stretch-card grid-margin">
                <div class="card bg-gradient-info card-img-holder text-white">
                    <div class="card-body">
                        <img src="/admin/assets/images/dashboard/circle.svg" class="card-img-absolute"
                            alt="circle-image" />
                        <h4 class="font-weight-normal mb-3">All Investments <i
                                class="mdi mdi-bookmark-outline mdi-24px float-right"></i>
                        </h4>
                        <h2 class="mb-5">{{ $users->sum('investment')}}</h2>
                        <h6 class="card-text">Increased by 50%</h6>
                    </div>
                </div>
            </div>
            <div class="col-md-4 stretch-card grid-margin">
                <div class="card bg-gradient-success card-img-holder text-white">
                    <div class="card-body">
                        <img src="/admin/assets/images/dashboard/circle.svg" class="card-img-absolute"
                            alt="circle-image" />
                        <h4 class="font-weight-normal mb-3">Returns <i class="mdi mdi-diamond mdi-24px float-right"></i>
                        </h4>
                        <h2 class="mb-5">{{$roi->sum('amount')}}</h2>
                        <h6 class="card-text">Increased by 5%</h6>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 grid-margin">
                <div class="card">
                    <div class="card-body">
                        @include('frontend.layout._error')
                        @include('frontend.layout._alert')
                        <h4 class="card-title">All Investors</h4>
                        <button class="btn btn-block btn-lg btn-gradient-primary mt-4 mb-5 float-left"  type="button" data-toggle="modal" data-target="#my-modal">+ Add an investor</button>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th> Profile </th>
                                        <th> Investment </th>
                                        <th> Investment ID </th>
                                        <th> View </th>
                                        <th> Delete </th>
                                        <th> Add Return </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($users as $user)
                                    <tr>
                                        <td>
                                            <img src={{ "https://ui-avatars.com/api/?name=".$user->lastname."+".$user->firstname."&background=9a55ff&color=fff&size=50" }} alt="image" class="mr-2">
                                            {{ $user->lastname.' '.$user->firstname }}
                                        </td>
                                        <td> {{ $user->investment }} </td>
                                        <td>
                                            {{ $user->investment_number }}
                                        </td>
                                        <td>
                                            <span class="page-title-icon bg-gradient-primary text-white mr-2">
                                                <i type="button" data-toggle="modal" data-target="#view{{$user->id}}" class="mdi mdi-eye user-table-icon"></i>
                                            </span>
                                        </td>
                                        <td>
                                            <span class="page-title-icon bg-gradient-danger text-white mr-2">
                                                <i type="button" data-toggle="modal" data-target="#delete{{$user->id}}" class="mdi mdi-delete-forever user-table-icon"></i>
                                            </span>

                                        </td>
                                        <td>
                                            <span class="page-title-icon bg-gradient-info text-white mr-2">
                                                <i type="button" data-toggle="modal" data-target="#return{{$user->id}}" class="mdi mdi-plus-circle-multiple-outline user-table-icon "></i>
                                            </span>
                                        </td>
                                    </tr>
                                    @include('admin.modals.view-investor-modal' , ['user' => $user])
                                    @include('admin.modals.delete-investor-modal',['user' => $user])
                                    @include('admin.modals.add-return-modal',['user' => $user])
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include('admin.modals.add-investor-modal')
        @include('admin.modals.add-admin')
    </div>
    <!-- content-wrapper ends -->
@endsection
