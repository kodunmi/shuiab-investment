<div id="my-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="my-modal-title" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="my-modal-title">Add new investor</h5>
                <button class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="forms-sample" method="POST" action="{{route('investor.create')}}">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleInputUsername1">firstname</label>
                        <input type="text" class="form-control" id="exampleInputUsername1" required placeholder="Username" name="firstname">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputUsername1">lastname</label>
                        <input type="text" class="form-control" id="exampleInputUsername1" required placeholder="Username" name="lastname">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" required placeholder="Email" name="email">
                    </div>
                    <div class="form-group">
                        <label for="iniinvst">Initial Investment</label>
                        <input type="number" class="form-control" id="iniinvst" required placeholder="Initial Invst" name="investment">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" class="form-control" id="exampleInputPassword1" required placeholder="Password" name="password">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputConfirmPassword1">Confirm Password</label>
                        <input type="password" class="form-control" id="exampleInputConfirmPassword1" required placeholder="Password" name="password_confirmation">
                    </div>
                    <div class="form-check form-check-flat form-check-primary">
                        <label class="form-check-label">
                            <input type="checkbox" class="form-check-input" name="generate"> Generate Investment Number </label>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-gradient-primary mr-2">Submit</button>
                    <button class="btn btn-light" data-dismiss="modal" aria-label="Close">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>
