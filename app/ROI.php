<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ROI extends Model
{
    protected $table = 'returns';

    protected $fillable = ['amount'];


    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
