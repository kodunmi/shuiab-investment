<?php

namespace App\Http\Controllers;

use App\Admin;
use App\ROI;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    public function index(){

       return view('admin.pages.dashboard', [
           'users' => User::all(),
           'roi' => ROI::all()
       ]);
    }

    public function createInvestor(Request $request){
        $request->validate([
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|email',
            'investment' => 'required|numeric',
            'password' => 'required|confirmed|min:8'
        ]);

        if($request->has('generate')){
            $investment_number = mt_rand(1000000000, 9999999999);
        }else{
            $investment_number = null;
        }

        User::create([
            'firstname' => $request->firstname,
            'lastname' => $request->lastname,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'investment' => $request->investment,
            'investment_number' => $investment_number
        ]);

        return back()->with([
            'message' => 'investor has been registered successfully',
            'type' => 'success'
        ]);
    }

    public function deleteInvestor($id){
        User::find($id)->delete();

        return back()->with([
            'message' => 'investor has been deleted successfully',
            'type' => 'success'
        ]);
    }

    public function addReturn(Request $request, $id){
        $request->validate([
            'amount' => 'required|numeric',
        ]);

        $return = new ROI([
            'amount' => $request->amount
        ]);

        User::find($id)->rois()->save($return);
        return back()->with([
            'message' => 'new return added',
            'type' => 'success'
        ]);

    }
    public function addAdmin(Request $request){

        $request->validate([
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:8|confirmed'
        ]);

        $admin = new Admin;
        $admin->firstname = $request->firstname;
        $admin->lastname = $request->lastname;
        $admin->email = $request->email;
        $admin->password = Hash::make($request->password);
        $admin->save();

        return back()->with([
            'message' => 'new admin registered successfully',
            'type' => 'success'
        ]);

    }
}
