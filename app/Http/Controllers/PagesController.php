<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index(){
        return view('frontend.pages.index');
    }
    public function contact(){
        return view('frontend.pages.contact');
    }

    public function dashboard(){
        return view('frontend.pages.dashboard');
    }
}
